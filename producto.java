
package Productos;

public class producto {
      //Atributos de la clase
    private int codProd;
    private String desc;
    private float pc;
    private float pv;
    private String um;
    private int cantProd;
    
    public producto(){
    this.codProd=0;
    this.desc="";
    this.pc=0.0f;
    this.pv=0.0f;
    this.um="";
    this.cantProd=0;
    }
//Constructor por argumentos
    public producto(int codProd, String desc, float pc, float pv, String um, int cantProd){
    this.codProd=codProd;
    this.desc=desc;
    this.pc=pc;
    this.pv=pv;
    this.um=um;
    this.cantProd=cantProd;
    }
    
    //Copia
    public producto(producto otro){
    this.codProd=otro.codProd;
    this.desc=otro.desc;
    this.pc=otro.pc;
    this.pv=otro.pv;
    this.um=otro.um;
    this.cantProd=otro.cantProd;
    }
    
    //Metodos Set y Get

    public int getCodProd() {
        return codProd;
    }

    public void setCodProd(int codProd) {
        this.codProd = codProd;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public float getPc() {
        return pc;
    }

    public void setPc(float pc) {
        this.pc = pc;
    }

    public float getPv() {
        return pv;
    }

    public void setPv(float pv) {
        this.pv = pv;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public int getCantProd() {
        return cantProd;
    }

    public void setCantProd(int cantProd) {
        this.cantProd = cantProd;
    }

    
    //Metodos de comportamiento
    public float calculoPrecioVenta(){
    float calculoPrecioVenta=0.0f;
    calculoPrecioVenta=this.pv*this.cantProd;
    return calculoPrecioVenta;
    }
    public float calculoPrecioCompra(){
    float calculoPrecioCompra=0.0f;
    calculoPrecioCompra=this.cantProd*this.pc;
    return calculoPrecioCompra;
    }
    public float calculoGanancia(){
    float calculoGanancia=0.0f;
    calculoGanancia=(this.calculoPrecioCompra()-this.calculoPrecioVenta());
    return calculoGanancia;
    }
    
}
